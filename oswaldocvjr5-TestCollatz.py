#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
        
        # corner cases
        # -1 is defined as a failure input
        v1 = collatz_eval(10, 4)
        self.assertEqual(v1, 20)
        
        v2 = collatz_eval(-6, -8)
        self.assertEqual(v2, -1)
        
        v3 = collatz_eval(-6, 1)
        self.assertEqual(v3, -1)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)
        
        v1 = collatz_eval(300, 340)
        self.assertEqual(v1, 144)
        
        v2 = collatz_eval(450, 456)
        self.assertEqual(v2, 54)
        
        v3 = collatz_eval(501, 546)
        self.assertEqual(v3, 137)
        
        
    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)
        
        v1 = collatz_eval(1000, 1034)
        self.assertEqual(v1, 156)
        
        v2 = collatz_eval(2078, 2101)
        self.assertEqual(v2, 126)
        
        v3 = collatz_eval(2301, 2378)
        self.assertEqual(v3, 183)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
        v1 = collatz_eval(5673, 5689)
        self.assertEqual(v1, 205)
        
        v2 = collatz_eval(608, 700)
        self.assertEqual(v2, 145)
        
        v3 = collatz_eval(7008, 7101)
        self.assertEqual(v3, 195)

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
