#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read2(self):
        s = "20 444\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 444)

    def test_read3(self):
        s = "123 456\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  123)
        self.assertEqual(j, 456)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(20, 444)
        self.assertEqual(v, 144)

    def test_eval_3(self):
        v = collatz_eval(123, 456)
        self.assertEqual(v, 144)
        
    def test_eval_4(self):
        v = collatz_eval(33, 78)
        self.assertEqual(v, 116)
        	
    def test_eval_5(self):
            v = collatz_eval(765, 900)
            self.assertEqual(v, 179)
            
    		
    def test_eval_6(self):
            v = collatz_eval(1000, 2006)
            self.assertEqual(v, 182)
            
    		
    def test_eval_7(self):
            v = collatz_eval(7690, 9800)
            self.assertEqual(v, 260)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 2, 1008, 179)
        self.assertEqual(w.getvalue(), "2 1008 179\n")
        
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 900432, 900000, 370)
        self.assertEqual(w.getvalue(), "900432 900000 370\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
            
    def test_solve2(self):
        r = StringIO("900432 900000\n20 444\n 123 456\n 33 78\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "900432 900000 370\n20 444 144\n123 456 144\n33 78 116\n")
            
    def test_solve3(self):
        r = StringIO("765 900\n1000 2006\n7690 9800\n578 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "765 900 179\n1000 2006 182\n7690 9800 260\n578 1000 179\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
